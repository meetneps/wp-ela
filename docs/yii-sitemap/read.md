# Yii 1.1.x Sitemap Generator


### How To Create a Sitemap for your Application?
 1.Download the Yii 1.1.x sitemap extension from [download here][http://www.yiiframework.com/extension/sitemapgenerator "YiiFramework"]

 2.Extract that files inside protected/extensions/ directory.The extracted directory structure sholud be protected/extensions/sitemapgenerator/SitemapGenerator.php.

 3.Next You have to set up sitemap configuration for your application . open a file called main.php inside 
 protected\config\main.php 

 copy the below code and paste it above the components array.

 ````
    'controllerMap'=>array(
            'sitemap'=>array(
                    'class'=>'ext.sitemapgenerator.SGController',
                    'cache'=>array('cache',60,null),
                    'config'=>array(
                            'sitemap.xml'=>array(
                                    'aliases'=>array(
                                            'application.controllers',
                                    ),

                            ),
                    ),
            ),
        ),
 ```
 4.Up to now  the sitemap configuration is finished, next you have to add your sitemap.xml file any URLs from 'application.controllehtrs' folder(protected\controllers). 

 5.Now If you open a Url tp://hostname/sitemap.xml it will display all the URL that you have created.

 #### How to create a sitemap for static page or view page ?
  1.Open any controller file ex: SiteController or what ever controller that you have created.

  syntax:
  ```
  /**  @sitemap dataSource=view:application.views.site.pages  
  */
  Options parameter:
  1.route=value - it will override the *Yii::app()->createAbsoluteUrl($route,$params);*
  2.dataSource=methodName - generate the url
  3.priority=value - it will override the default priority(0.5) of sitemap generator.
  4.changefreq=value - it will overrides default changefreq value for sitemap,
  5.lastmod=value - it will override the default last modification of sitemap generator (today is default).  
  Example:
  ```
    class SiteController extends Controller
	{
	
		
		/**  @sitemap dataSource=view:application.views.site.pages route=site/page 
		*/

		public function actions()
		{
			return array(

				// page action renders "static" pages stored under 'protected/views/site/pages'
				
				'page'=>array(
					'class'=>'CViewAction',
				),
			);
		}
	}
  ```
   The parameter must be space-separated 
   ex: 
   ```
    route= not route =
   ```
   dataSource parameter must be view:[aliases], the aliases can be a your application static site pages.
   static site page mean about,help,faq pages etc.
  
 2.now the page url display like     http://yourhost/site/page/view/about
  
  Here about is coming from protected\views\site\pages

  if you want to change *view* to *com* or another name in URL, we need one parameter called *params*
  syntax : params=view:[aliases]
  ex:
  ```
  /**  @sitemap dataSource=view:application.views.site.pages 
   params=view:com route=site/page 
		*/
  ```
  Now the url is  http://yourhost/site/page/com/about

#### How To Create dynamic urls?
1.In this section we are creating urls based on the value from database.For example you need to create a url like some book name or author name or username etc.
ex:
 http://yourhost/profile/john,
 http://yourhost/profile/john-mathew

2.For that you need to do this following steps: In controller

 ```
    /**
     * @sitemap dataSource=getSlugUrl route=view/
    */

    //get slug url from database
    public function getSlugUrl(){

    	$models = Yourmodel::model()->findAll();
    	$data=array();
		foreach($models as $model)
			$data[]=array(
				'route'=>'profile/'.$model->slug_url,
			    'changefreq'=>'monthly',
				'priority'=>0.7,
				'lastmod'=>$model->date_modified, );

		return $data;

    }

 ```
 Now the url will be 
 1.http://yourhost/profile/john
 2.http://yourhost/profile/rasmus-john or 
 what are user registered in your database.

