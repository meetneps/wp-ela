# Wordpress Theme

### Create Wordpress Theme :
1.Before start to create a Wordpress theme you need to have a good knowledge in HTML, CSS and PHP.Because wordpress fully developed by HTML, CSS and PHP.

2.If your not familiar with these, take this as a opportunity to improve your coding skills.

3.Before start to create a theme you have to take a look to read the wordpress coding standard.

	1.HTML coding standard[click here]['https://make.wordpress.org/core/handbook/best-practices/coding-standards/html/']
	2.CSS coding standard[click here]('https://make.wordpress.org/core/handbook/best-practices/coding-standards/css/')
	3.PHP coding standard[click here]('https://make.wordpress.org/core/handbook/best-practices/coding-standards/php/')
	4.Javascript coding standard[click here]('https://make.wordpress.org/core/handbook/best-practices/coding-standards/javascript/')

4.After read this let start to create a wordpress theme.
	1.The wordpress theme directory present in directory of wp-content/themes,So you have to create a new subdirectory inside a themes folder like **newtheme** or whatever you like.
  	2.Template File List:
    	1.index.php
        	
   This is the main template file in theme.
```<?php get_header();?>
	<h2>Main content goes here</h2>
	<div>
		<p>content goes here.</p>
	</div>
   <?php get_footer(); ?>
```

        	get_header() function get the header.php file.
        	get_footer() function get the footer.php file.

     	2.style.css
        	This is main style sheet file you must create this file.It contains details of your wordpress theme.
```
        /*
			Theme Name: newtheme
			Theme URI: http://wordpress.org/themes/newtheme
			Author: Wordpress team
			Author URI: http://wordpress.org/
			Description: New Theme
			Version: 1.0
			License: GNU General Public License v2.0
			License URI: http://www.gnu.org/licenses/gpl-2.0.html
			Tags: one-column, two-columns, right-sidebar,  custom-header, custom-menu, editor-style, featured-images, microformats, post-formats, rtl-language-support, sticky-post, translation-ready
			Text Domain: twentythirteen
			This theme, like WordPress, is licensed under the GPL.
			Use it to make something cool, have fun, and share what you've learned with others.
			*/
```
     	3.header.php
        	It contains head part of the wordpress theme.
```
        <!DOCTYPE html>
		<html <?php language_attributes(); ?>>
		    <head>
		        <meta charset="<?php bloginfo( 'charset' ); ?>" />        
		        <?php wp_head(); ?>
		    </head>
		    <body <?php body_class(); ?>> 
			<div id="header">
               <p>head part</p>
			</div>

```
        	you don't need to close the body and html tag in header file.
      	4.footer.php
        	It contains the footer part of the theme.
```
	        <footer>
				<p>footer content goes here</p>
			</footer>
			<?php wp_footer(); ?>
			</body>
			</html>
```
        5.functions.php
        	It modify the core elements of theme ant it will be load before the content.

      	6.sidebar.php
         	This sidebar is widgetized area of your theme.
         [for detailed explanation]('https://developer.wordpress.org/themes/functionality/sidebars/')

     	7.single.php
        	If you want to show  particular post or page separately.     

     	8.comments.php 
        	This is comment template.If you want to change your default comment template create a comments.php file.
     	9.404.php
        	This is page not found template.You can redesign and change the default 404 template.

    *After finish above steps you have to activate your theme in Apperance->Themes.
    ![alt text](http://localhost/documentation/wordpress/wp-img/newtheme.png "Theme Name")
    * It will list some default wordpress themes from that you can able to see your newly created theme called **newtheme**.So if you hover on newtheme click **Theme Details** it will display the details of your theme.
    Then click **Activate** or **Live Preview** to apply your theme.
     ![alt text](http://localhost/documentation/wordpress/wp-img/activate.png "activate new theme")
	3.How to include CSS and JavaScript for your template?
	    1.first create file called functions.php and create two subdirectory inside newtheme directory called js and css.

	    ![alt text](http://localhost/documentation/wordpress/wp-img/css.png "create css and js folder")
	    functions.php
```
	     <?php
		  function newtheme_script_enqueue(){
		 	wp_enqueue_style('customstyle',get_template_directory_uri().'/css/newtheme.css');
		 	
		    wp_enqueue_script('script',get_template_directory_uri().'/js/newtheme.js',array(),false,true);
		 }
		 add_action('wp_enqueue_scripts','newtheme_script_enqueue');

		?>
```
	    2.wp_enqueue_style, wp_enqueue_script is used for load css and js for our newly created theme.

		3.Then we have to load our css and js in template For that you have to use wp_head() hook in your head section in header.php file it will load all css file that your created and wp_footer() hook in your footer section footer.php file it will load all js file that you have created.

    4.Create wordpress widget:

       WordPress Widgets add content and features to your sidebar. Examples are the default widgets that come with WordPress; for Categories, Tag cloud, Search, etc. Plugins will often add their own widgets.

      1.first you have to register sidebar widget in functions.php
```<?php function arphabet_widgets_init(){
		register_sidebar( array(
				'name'          => 'Right sidebar',
				'id'            => 'right_1',
				'before_widget' => '<div>',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="rounded">',
				'after_title'   => '</h2>',
			) );

	}
	add_action( 'widgets_init', 'arphabet_widgets_init' );
    ?>
``` 
        * After register a widget if you open admin dashboard widget option will appear in Appearance->widgets.So if click it will show Available widget on left side and right side what are the widgets added in your theme.you can drag any item on left side and drop it to right side.
         ![alt text](http://localhost/documentation/wordpress/wp-img/newwidgetside.png "Add widget")
	    * register_sidebar –  registering a sidebar for yout theme
	    * 'name' => __( 'Right sidebar', 'mytheme' ), – is the widget area’s name that will appear in Appearance -> Widgets

	    * 'id' => 'right-1' – assigns an ID to the sidebar. WordPress uses ‘id’ to assign widgets to a specific sidebar.
	    * before_widget/after_widget –  By default, WordPress sets these as list items but in the above example they have been altered to div.
	    * before_title/after_title – the wrapper elements for the widget’s title. By default, WordPress sets it to h2. 

       2.Display sidebar in Your Theme
          1.After register a sidebar widget you have to create sidebar.php file,inside that file you have to write a below code.
```
            <?php if ( is_active_sidebar( 'right_1' ) ) : ?>
				<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
					<?php dynamic_sidebar( 'right_1' ); ?>
				</div>
			<?php endif; ?>

``` 
          2.is_active_sidebar($params) is used to check if the sidebar is active or not.
          3.dynamic_sidebar($params) is used to display the sidebar.
          4.get_sidebar() is used to load your sidebar.
          5.get_sidebar('right_1') to load particular widget.
          ![alt text](http://localhost/documentation/wordpress/wp-img/complete.png "Display widget")

    5.Creating Navigation Menu For your Theme	
	    To create menu, first you need to add pages for that menu like about,contact etc.in admin panel pages->Add New 
        1.Navigation Menus are customizable menus in your theme. They allow users to add Pages, Posts, Categories, and URLs to the menu. To create a navigation menu you’ll need to register it, and then display the menu in the appropriate location in your theme.
        2.Register Menu:
        	Add this code to the top of widget_init.
            in functions.php
  ```
            	function newtheme_custome_menus(){
				 	add_theme_support('menus');
				 	 register_nav_menus(
				    array(
				      'header-left-menu' => __( 'Header Left Menu' ,'newtheme'),
				      'header-right-menu' =>__('Header Right Menu','newtheme'),
				      'footer-menu' => __( 'Footer Menu','newtheme' ),
				      'header-bottom' =>__('Header Bottom Menu','newtheme')

					    )
					  );
			 	}
			 add_action('init','newtheme_custome_menus');

```
           This  name that will appear at Appearance -> Menus.           
           ![alt text](http://localhost/documentation/wordpress/wp-img/newmenu.png "Create Menu")
           1.add_theme_support('menus') to add menu option in your theme.
           2.register_nav_menus()-to register menu in your theme
        3.Display menu:
            To display menu that you have created in Appearance-> Menus use wp_nave_menu() display in your theme.You can display this menu where ever you want ex.display in header section or footer section or any where.
             ![alt text](http://localhost/documentation/wordpress/wp-img/newmenusetting.png "Menu location select")
           ```
           <?php wp_nav_menu( array( 'theme_location' => 'header-left-menu' ) ); ?>

           ```           
    6.How to Use Post Loop
        To process loop first you have create some post in post->Add New
        1.Loop is a PHP code to display wordpress posts.It will process each posts and you can style that using HTML tag.
		Ex:
```
			   <?php 
			    if ( have_posts() ) {
			        while ( have_posts() ) {
			                the_post(); 
			                //
			                // Post Content here
			                //
			        } // end while
			   } // end if
			   ?>
```
			*if(have_posts()) - check whether the posts are discovered or not.
			*while(have_posts())-iterate next item.
			*the_posts()-retrieve the content.
			display the post title use <?php the_title(); ?>
			disply the post category use <?php the_category();?>
			get author name <?php the_author_link(); ?>
			The code will be placed in index.php file or other template file.
			ex:
```
			   <div>
					<?php if(have_posts()): ?>
					    <?php while(have_posts()): ?>
					    <?php  the_post(); ?>				    
							<div class="post-content">
						    	<ul>
								    <li><h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2></li>
								    <li><h5 class="author-details">Posted by <span class="author-title"><?php the_author_link(); ?></span></h5></li>
								    <?php if (has_post_thumbnail()): ?>
										<li><?php the_post_thumbnail();?></li>
									<?php endif; ?>
								    <li><p class="description"><?php the_content(); ?></p></li>
							    </ul>
						    </div>
						<?php endwhile; ?>
					<?php endif; ?>
				    
				</div>
				<?php get_footer(); ?>

```

    7.404 page Template
        404.php file is used to page not found error.This template will appear if user search post or category  not exists or not created yet.
    #### Create Single Page Template
    first you need to create single.php file under the newtheme directory.
    If you see main page and single page you get the difference.What is the difference??? 
    Yes this is difference i am created separate image for single page.So like that you can change the layout of the page.
    
    single.php
    ```
    <?php get_header(); ?>
<div class="single-page-img">
	<img class="single-img" src="<?php echo esc_url( get_template_directory_uri()); ?>/images/single-page-img.png" alt="image"/>
</div> 
<div class="single-page">
	<div class="post-content">
			<?php if(have_posts()): ?>
			    <?php while(have_posts()): ?>
			    <?php  the_post(); ?>
		    	<ul>
				    <li><h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2></li>
				    <li><h5 class="author-details">Posted by <span class="author-title"><?php the_author_link(); ?></span></h5></li>
				    <?php if (has_post_thumbnail()): ?>
						<li><?php the_post_thumbnail();?></li>
					<?php endif; ?>
				    <li><p class="description"><?php the_content(); ?></p></li>
			    </ul>
		    	<?php endwhile; ?>
		    <?php else: ?>
		    <?php get_template_part('404'); ?>
			<?php endif; ?>
    </div>		
    <div class="sidebar">
    	<?php get_sidebar(); ?>
    </div>
</div>
<?php get_footer(); ?>
    ```



Final screenshot of Theme
