<?php

add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' ); 
$args = array(
    'default-color' => 'ffffff',   
);
add_theme_support( 'custom-background', $args );

add_editor_style( 'css/editor-style.css' );


$args = array(   
    'height'        => 45,    
);
add_theme_support( 'custom-header', $args );


function pfstheme_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'pfstheme_content_width', 840 );
}
add_action( 'after_setup_theme', 'pfstheme_content_width', 0 );

load_theme_textdomain('pfstheme');

 function pfstheme_script_enqueue(){
 	wp_enqueue_style('customstyle',get_template_directory_uri().'/css/pfstheme-latest.css');
 	

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
    wp_enqueue_script('script',get_template_directory_uri().'/js/app.js',array(),false,true);
 }
 add_action('wp_enqueue_scripts','pfstheme_script_enqueue');
 function pfstheme_custome_menus(){
 	add_theme_support('menus');
 	 register_nav_menus(
    array(
      'header-left-menu' => __( 'Header Left Menu' ,'pfstheme'),
      'header-right-menu' =>__('Header Right Menu','pfstheme'),
      'footer-menu' => __( 'Footer Menu','pfstheme' ),
      'header-bottom' =>__('Header Bottom Menu','pfstheme')

    )
  );
 }
 add_action('init','pfstheme_custome_menus');
 
 function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Home right sidebar',
		'id'            => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );
function footer_side_bar() {

  register_sidebar( array(
    'name'          => 'Footer sider bar',
    'id'            => 'footer_bar',
    'before_widget' => '<div class="footer-list-content">',
    'after_widget'  => '</div>',
    'before_title'  => '<h2 class="rounded">',
    'after_title'   => '</h2>',
  ) );

}
add_action( 'widgets_init', 'footer_side_bar' );

// function header_search_list(){
//     register_sidebar(array(
//         'name' => 'Header Search list',
//          'id' => 'header-search',
//          'before_widget'=>'<div class="header-search-list">',
//          'after_widget'=>'</div>'

//         ));

// }
// add_action( 'widgets_init', 'header-search-list' );
// function remove_more_link_scroll( $link ) {
// 	$link = preg_replace( '|#more-[0-9]+|', '', $link );
// 	return $link;
// }
// add_filter( 'the_content_more_link', 'remove_more_link_scroll' );

function modify_read_more_link() {

    return '<div class="read-more-content text-center"><a class="more-link" href="' . get_permalink() . '">read more  <i class="fa fa-long-arrow-right" aria-hidden="true"></i>

		</a></div>';
}
add_filter( 'the_content_more_link', 'modify_read_more_link' );


// Modify comments header text in comments
// add_filter( 'genesis_title_comments', 'child_title_comments');
// function child_title_comments() {
//     return __(comments_number( '<h3>No Responses</h3>', '<h3>1 Response</h3>', '<h3>% Responses...</h3>' ), 'pfstheme');
// }
 
// Unset URL from comment form
// function crunchify_move_comment_form_below( $fields ) { 
//     $comment_field = $fields['comment']; 
//     unset( $fields['comment'] ); 
//     $fields['comment'] = $comment_field; 
//     return $fields; 
// } 
// add_filter( 'comment_form_fields', 'crunchify_move_comment_form_below' ); 
 
function custom_comments( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' :
        if ( 'div' == $args['style'] ) {
            $tag = 'div';
            $add_below = 'comment';
        } else {
            $tag = 'li';
            $add_below = 'div-comment';
        }
    ?>
    <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
        <p><?php _e( 'Pingback:', 'pfstheme' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'pfstheme' ), '<span class="edit-link">', '</span>' ); ?></p>
    <?php
            break;
        default :
        global $post;
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
                <footer class="comment-meta">
                    <div class="avatar">
                      <?php if ( 0 != $args['avatar_size'] ) echo get_avatar( $comment, $args['avatar_size'] ); ?>

                    </div>
                    <div class="comment-author vcard">
                        
                        <?php printf( __( '%s <span class="says">says:</span>','pfstheme' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
                         <div class="reply">
                            <?php comment_reply_link( array_merge( $args, array( 'add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                        </div><!-- .reply -->
                    </div><!-- .comment-author -->

                    <div class="comment-metadata">
                        <a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
                            <time datetime="<?php comment_time( 'c' ); ?>">
                                <?php printf( _x( '%1$s at %2$s', '1: date, 2: time','pfstheme' ), get_comment_date(), get_comment_time() ); ?>
                            </time>
                        </a>
                        <?php edit_comment_link( __( 'Edit', 'pfstheme' ), '<span class="edit-link">', '</span>' ); ?>
                       
                    </div><!-- .comment-metadata -->

                    <?php if ( '0' == $comment->comment_approved ) : ?>
                    <p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'pfstheme' ); ?></p>
                    <?php endif; ?>
                </footer><!-- .comment-meta -->

                <div class="comment-content">
                    <?php comment_text(); ?>
                </div><!-- .comment-content -->

               
            </article><!-- .comment-body -->
    <?php
        break;
    endswitch; 

}


?>