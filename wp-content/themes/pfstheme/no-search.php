<?php
  get_header(); 
?>
<div class="no-search-found">
  <h1>Nothing Found</h1>	
  <p>Sorry,but nothing matched your seaech terms.Please try again with some different keywords.</p>
	<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<label>
			<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'pfstheme' ); ?></span>
			<input type="search" class="no-search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'pfstheme' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
		</label>
		<div class="search-btn-div">
			<button type="submit" class="search-submit"><span class="screen-text"><?php echo _x( 'Search', 'submit button', 'pfstheme' ); ?></span></button>
		</div>
		
	</form>
</div>

