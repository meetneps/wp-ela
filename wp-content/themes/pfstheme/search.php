<?php
 get_header();
 get_template_part('theme-image');
?>

<div class="search-list-con single-page-content">
		<div id="primary" class="content-area loop-content">
			<div id="main" class="site-main" role="main">

			<?php if ( have_posts() ) : ?>

				<!-- <header class="page-header"> -->
					<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'pfstheme' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
				<!-- </header> --><!-- .page-header -->

				<?php
				// Start the loop.
				while ( have_posts() ) : the_post(); ?>

					
					 <div class="body-content">
			    	<div class="date-div text-center">	
			    	    <div class="date-div-1 text-center">
			    	    	<span class='month '><?php echo get_the_date('M');?></span>
						    <span class='date'><?php echo get_the_date('j'); ?></span>
			    	    </div>
						  <p class="comments"><?php 
						   echo  comments_number('Comments Off','<i class="fa fa-twitch" aria-hidden="true"></i> 1','<i class="fa fa-twitch" aria-hidden="true"></i> %');
						     
						  ?>
						</p>
						
			    	</div>
			    	<div class="post-content">    		
				    	<ul>

						    <li><h2 class="title"><?php the_title(); ?></h2></li>
						    <li><h6 class="author-details">Posted by <span class="author-title"><?php the_author_link(); ?></span></h6></li>
						    
						    <?php if (has_post_thumbnail()): ?>
								<li><?php the_post_thumbnail();?></li>
							<?php endif; ?>
							
						    <li><p class="description"><?php the_content(); ?></p></li>
						    <li><i class="fa fa-folder" aria-hidden="true"><?php the_category(' ');?></i></li>
						    <?php if(has_tag()):   ?>
						    <li><i class="fa fa-tags" aria-hidden="true"><?php the_tags('','<span>,</span>','') ?></i></li>
	                        <?php endif; ?>
							<li class="comment-link"><a href="<?php comments_link(); ?>"> <i class="fa fa-twitch" aria-hidden="true"></i> Comment</a></li>
					    </ul>
				    </div>
			
			    </div>
			    <hr>
			    <div class="previous-post-div text-center">
					 <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
					<?php previous_post_link("%link","%title"); ?>  
				</div>
				<?php 

		   //         if ( comments_open() || get_comments_number() ) {
					// 	comments_template();
						
					// }
				?>                    
				
				<?php endwhile; ?>				
			
			<?php else : 
			// If no content, include the "No posts found" template.
				get_template_part('no-search');
             ?>
			<?php endif; ?>
		

			</div><!-- .site-main -->
	</div><!-- .content-area -->

	<div class="sidebar-content">
			<?php get_sidebar(); ?>
	</div>

</div>
<?php get_footer(); ?>
