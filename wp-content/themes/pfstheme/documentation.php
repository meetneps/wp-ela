<body <?php body_class(); ?>>
	Display the classes for the body element.
	ex:
	<body <?php body_class( 'class-name' ); ?>>
		The results would be:
     <body class="class-name">	
</body>
<?php language_attributes( string $doctype = 'html' ) ?>

Displays the language attributes for the html tag.
ex:<html lang="html">

add_theme_support( string $feature )
Registers theme support for a given feature.

add_editor_style( array|string $stylesheet = 'editor-style.css' )

Add callback for custom TinyMCE editor stylesheets.


TinyMCE is the name of the visual editor that comes with WordPress, which can be used to edit post and page content. It comes with a variety of buttons for standard HTML tags like Strong, Emphasis, Blockquote and Lists
