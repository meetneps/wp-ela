

<!-- 146
$post_12 = get_post(12); 

$content = $post_12->post_conten -->
   
 	<footer class="footer-part"> 
      <div class="footer-content">
        <div class="menu-sider-bar">
       	  <div class="menu-footer-menu-list-container">
            <h2>About</h2>
             <?php 
                
                //$id=146; 
                //$post = get_post($id);
                $post = get_page_by_title( 'About'); 
                $content = apply_filters('the_content', $post->post_content); 
                  echo "<p>$content</p>";

                ?>
              
          </div>
          <?php if ( is_active_sidebar( 'footer_bar') ) : ?>
      		<div id="footer-sidebar" class="footer-sidebar-div widget-area" role="complementary">
      			<?php dynamic_sidebar( 'footer_bar' ); ?>          
      		</div>
        <?php endif; ?>
        </div>
        <div class="custom-links">
             <h2>Custom Links</h2>
             <ul>
               <li><a href="#">Amet impedit</a></li>
               <li><a href="#">Perferendis doloribus</a></li>
               <li><a href="#">Laboriosam at magni</a></li>
               <li><a href="#">Qui molestias quia</a></li>
               <li><a href="#">Aliquid repellat accu</a></li>
               <li><a href="#">Officiis ratione dolor</a></li>
               <li><a href="#">Dolorum est earum</a></li>
             </ul>
        </div>    
    	
         
      </div>
      <div class="arrow-top-div">
        <a href="#header" class="arrow-up">
         <i class="fa fa-angle-up fa-2x" aria-hidden="true"></i></a>
      </div>
      <div class="copy-right">
           <p><span class="ascent-footer">&copy; 2016 ascent.All rights reserved</span> <span class="ascent-white"> | Ascent by </span><span class="ascent-footer">NetTantra </span></p>
      </div>
   </footer>
 
  
  </div>
  <?php wp_footer(); ?>
 </body>
</html>