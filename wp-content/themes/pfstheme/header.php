
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />        
        <?php wp_head(); ?>
    </head>
<body <?php body_class(); ?>> 
	<div id="header"></div>
	<div class="main-content">
        
		<header>
			<!-- <p>header part</p> -->
            <div class="menu-social-menu-container">
            	<ul id="menu-social-menu">
            		<li><a href="<?php echo esc_url("https://www.freecall.com/login");?>"><i class="fa fa-phone" aria-hidden="true"></i> +1-408-658-0677</a></li>
            		<li><a href="mailto:support@nettantra.com"><i class="fa fa-envelope" aria-hidden="true"></i>
support@nettantra.com</a>
            		</li>
            	</ul>
            </div>
			
		    <div class="menu-social-right-menu-container">
		    	<ul id="menu-social-right-menu">

			    	<li><a href="<?php echo esc_url( "https://twitter.com/login" ); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			    	<li><a href="<?php echo esc_url("https://www.facebook.com/login");?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			    	<li><a href="<?php echo esc_url("https://plus.google.com/");?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
			    	<li><a href="<?php echo esc_url("https://github.com/login");?>"><i class="fa fa-github" aria-hidden="true"></i></a></li>
			    </ul>
		    </div>
		   <?php  //wp_nav_menu( array( 'theme_location' => 'header-left-menu' ) ); ?>
			<?php //wp_nav_menu(array('theme_location' =>'header-right-menu')); ?>	
		</header>	
	
		<div id="header-list">	
			<div class="header-left">
				
				<h1 class="blog-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>	

				<?php $description = get_bloginfo( 'description', 'display' ); ?>
				<?php if ( $description || is_customize_preview() ) : ?>
					<p class="site-description"><?php echo $description; ?></p>
			    <?php endif; ?>
			</div>
			<div class="header-bottom-menu">
		    	<?php wp_nav_menu(array('theme_location'=>'header-bottom')); ?>
		    </div>  
		    <div class="search-details">
		    	<button class="search-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
		    </div> 	    
			  
		</div> 
		  
		<?php  //if(is_search()): ?>   
		<div class="search-list-div">
		   <form role="search" method="get" class="search-form" action="<?php echo site_url() ?>">
				<label>	
				<i class="fa fa-search" aria-hidden="true"></i>				
					<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'pfstheme' ); ?>" value="" name="s">
				</label>	
			</form>
		</div> 

	    <?php //endif; ?>
	
	
	
