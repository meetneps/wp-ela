<?php
  get_header();
  get_template_part('theme-image');
?>
<div class="error text-center">
	<h1 class="error-content">404</h1>
	<h3 class="error-content">Oops! Something Went Wrong here.</h3>
	<h4>Nothing could be found this location.</h3>
    <h4>Trying Go back to <span id="error-home-page">HomePage</span> instead?</h4>
	
</div>
<div class="ruler-404">
	<hr>
</div>


<?php get_footer(); ?>