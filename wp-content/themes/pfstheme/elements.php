<div class="elements-div">
	<h1>Elements</h1>
	<h3>Table format</h3>
	<div>
		<table class="user-info">
		  <tr>
		  	<th>#</th>
		    <th>First name</th>
		    <th>Last name</th>
		    <th>User name</th>
		  </tr>  
		  <tr>
		  	<td>1</td>
		    <td>Ela</td>
		    <td>Mathi</td>
		    <td>@fat</td>
		  </tr>
		  <tr>
		  	<td>2</td>
		    <td>Jane</td>
		    <td>Doe</td>
		    <td>@jow</td>
		  </tr>
		  <tr>
		  	<td>3</td>
		    <td>Jane</td>
		    <td>Doe</td>
		    <td>@jam</td>
		  </tr>
		</table>
	</div>
	<div class="read-more-btn-div ">
		<div class="text-center first-div">
			<button class="green">read more</button>
			<button class="black">read more</button>
			<button class="red">read more</button>
			<button class="orange">read more</button>
		</div>
		<div class="text-center second-div">
			<button class="yellow">read more</button>
			<button class="lite-green">read more</button>
			<button class="white">read more</button>
			<button class="ash">read more</button>
		</div>
	</div>
	<hr>
	<div>
		<p>Contact form plugin is easiest way to build any responsive form for your website. No fuss or messing with code create the form you want easily with simple drag & drop form builder.</p>
		<p>With contact form plugin you can create simple contact forms or complex application form easily in mere minutes.</p>
		<p>Contact form plugin is easiest way to build any responsive form for your website. No fuss or messing with code create the form you want easily with simple drag & drop form builder.</p>
		<p>Whether you are a beginner or advanced WordPress user contact form plugin will certainly fulfill all your needs and requirements.</p>
	</div>
	<div>
		<table class="user-info">
		  <tr>
		  	<th>#</th>
		    <th>First name</th>
		    <th>Last name</th>
		    <th>User name</th>
		  </tr>  
		  <tr>
		  	<td>1</td>
		    <td>Ela</td>
		    <td>Mathi</td>
		    <td>@fat</td>
		  </tr>
		  <tr>
		  	<td>2</td>
		    <td>Jane</td>
		    <td>Doe</td>
		    <td>@jow</td>
		  </tr>
		  <tr>
		  	<td>3</td>
		    <td>Jane</td>
		    <td>Doe</td>
		    <td>@jam</td>
		  </tr>
		</table>
	</div>
	<hr>
	<div class="grid">
		<h3>Grid</h3>
		<div class="two-grid">
			<div class="two-child-1">
				<h5>One Half</h5>
				<p>WordPress Themes are files that work together to create the design and functionality of a WordPress site. Each Theme may be different, offering many choices for site owners to instantly change their website look.</p>
				<p>WordPress Themes live in subdirectories of the WordPress themes directory (wp-content/themes/ by default) which cannot be directly moved using the wp-config.php file. The Theme's subdirectory holds all of the Theme's stylesheet files, template files, and optional functions file (functions.php), JavaScript files, and images. For example, a Theme named "test" would reside in the directory wp-content/themes/test/. Avoid using numbers for the theme name, as this prevents it from being displayed in the available themes list. </p>
			</div>
			<div class="two-child-2">
				<h5>One Half</h5>
				<p>WordPress Themes are files that work together to create the design and functionality of a WordPress site. Each Theme may be different, offering many choices for site owners to instantly change their website look.</p>
				<p>WordPress Themes live in subdirectories of the WordPress themes directory (wp-content/themes/ by default) which cannot be directly moved using the wp-config.php file. The Theme's subdirectory holds all of the Theme's stylesheet files, template files, and optional functions file (functions.php), JavaScript files, and images. For example, a Theme named "test" would reside in the directory wp-content/themes/test/. Avoid using numbers for the theme name, as this prevents it from being displayed in the available themes list. </p>
			</div>
			
		</div>
		<div class="three-grid">
			<div class="three-grid-child-1">
				<h5>One thrid</h5>
				<p>This article is about developing WordPress Themes. If you wish to learn more about how to install and use Themes, review Using Themes. This topic differs from Using Themes because it discusses the technical aspects of writing code to build your own Themes rather than how to activate Themes or where to obtain new Themes</p>
			</div>
			<div class="three-grid-child-2">
				<h5>One thrid</h5>
				<p>This article is about developing WordPress Themes. If you wish to learn more about how to install and use Themes, review Using Themes. This topic differs from Using Themes because it discusses the technical aspects of writing code to build your own Themes rather than how to activate Themes or where to obtain new Themes</p>
			</div>
			<div class="three-grid-child-3">
				<h5>One thrid</h5>
				<p>This article is about developing WordPress Themes. If you wish to learn more about how to install and use Themes, review Using Themes. This topic differs from Using Themes because it discusses the technical aspects of writing code to build your own Themes rather than how to activate Themes or where to obtain new Themes</p>
			</div>
		</div>

	</div>
</div>

