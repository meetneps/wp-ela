

<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
	<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'home_right_1' ); ?>
	</div>
<?php endif; ?>

<!-- <div id="sidebar">

    <?php //get_template_part('archives');?>

    <?php //get_template_part('get_recent_post'); ?>
 	
  	<?php //get_template_part('category');?>
  	<?php //get_template_part('meta'); ?>
   
</div> -->