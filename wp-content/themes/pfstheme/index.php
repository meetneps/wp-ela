<?php
  get_header();
 if(is_page('elements') || is_page('support')){
    get_template_part('theme-image');
 }else{
 	get_template_part('slider');
 }
?>

<div class="sub-content">
	<?php if(is_page('elements')): ?>
			    <?php get_template_part('elements'); ?>
    <?php else: ?>
		<div class="loop-content">

			<?php if(is_page('support')):?>
			    
				<?php if(have_posts()):?>
					<?php while (have_posts()):?> 
					<?php the_post(); ?>					
					<div class="support-form-content">
						<h2><?php the_title(); ?></h2>
						<?php the_content(); ?>
					</div>
				   <?php endwhile ?>
				<?php endif; ?>				
		    <?php else:?>
				<?php if(have_posts()): ?>
				    <?php while(have_posts()): ?>
				    <?php  the_post(); ?>
				    
				    <div class="body-content" id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
				    	<div class="date-div text-center">	
				    	    <div class="date-div-1 text-center">
				    	    	<span class='month '><?php echo get_the_date('M');?></span>
							    <span class='date'><?php echo get_the_date('j'); ?></span>
				    	    </div>
							  <p class="comments"><?php 
							   comments_number('Comments Off','<i class="fa fa-twitch" aria-hidden="true"></i> 1','<i class="fa fa-twitch" aria-hidden="true"></i> %');
							     
							  ?>
							</p>
							
				    	</div>
				    	<div class="post-content">    		
					    	<ul>

							    <li><h2 class="title"><?php the_category(' '); ?>: <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2></li>
							    <li><h5 class="author-details">Posted by <span class="author-title"><?php the_author_link(); ?></span></h5></li>
							    
							    <?php if (has_post_thumbnail()): ?>
									<li><?php the_post_thumbnail();?></li>
								<?php endif; ?>
								
							    <li><p class="description"><?php the_content(); ?></p></li>
							    <li><i class="fa fa-folder" aria-hidden="true"><?php the_category(' ');?></i></li>
							    <?php if(has_tag()):   ?>
							    <li><i class="fa fa-tags" aria-hidden="true"><?php the_tags('','<span>,</span>','') ?></i></li>
		                        <?php endif; ?>
								<li class="comment-link"><a href="<?php comments_link(); ?>"> <i class="fa fa-twitch" aria-hidden="true"></i> Comment</a></li>
						    </ul>
					    </div>
				
				    </div>
				    <hr>
				    <?php endwhile ?>
				    <?php  wp_link_pages(); ?>
				   <?php // Previous/next page navigation.
					the_posts_pagination( array(
						'prev_text'          => __( 'Previous page', 'pfstheme' ),
						'next_text'          => __( 'Next page', 'pfstheme' ),
						'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'pfstheme' ) . ' </span>',
					) ); ?>
				<?php else: 
				
				get_template_part('404'); ?>
				<?php  endif; ?>
		    <?php endif; ?>
		</div>
		<div class="sidebar-content">
			<?php  
			get_template_part('sidebar'); ?>
		</div>
    <?php endif;?>
</div>
<?php 
  get_footer();
?>

