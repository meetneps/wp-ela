<?php


get_header(); 
get_template_part('theme-image');
?>
<div class="category-div">
	<div class="category-sub">
		<!-- <section id="primary" class="content-area"> -->
			<div id="content" class="site-content" role="main">

				<?php if ( have_posts() ) : ?>

				
					<h1 class="archive-title"><?php printf( __( 'Category Archives: %s', 'pfstheme' ), single_cat_title( '', false ) ); ?></h1>

					<?php
						// Show an optional term description.
						$term_description = term_description();
						if ( ! empty( $term_description ) ) :
							printf( '<div class="taxonomy-description">%s</div>','pfstheme', $term_description );
						endif;
					?>
				

				<?php while(have_posts()): ?>
					    <?php  the_post(); ?>			    
					    <div class="body-content">
					    	<div class="date-div text-center">	
					    	    <div class="date-div-1 text-center">
					    	    	<span class='month '><?php echo get_the_date('M');?></span>
								    <span class='date'><?php echo get_the_date('j'); ?></span>
					    	    </div>
								  <p class="comments"><?php 
								   echo  comments_number('Comments Off','<i class="fa fa-twitch" aria-hidden="true"></i> 1','<i class="fa fa-twitch" aria-hidden="true"></i> %');
								     
								  ?>
								</p>
								
					    	</div>
					    	<div class="post-content">    		
						    	<ul>

								    <li><h2 class="title"><?php the_category(' '); ?>: <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2></li>
								    <li><h6 class="author-details">Posted by <span class="author-title"><?php the_author_link(); ?></span></h6></li>
								    
								    <?php if (has_post_thumbnail()): ?>
										<li><?php the_post_thumbnail();?></li>
									<?php endif; ?>
									
								    <li><p class="description"><?php the_content(); ?></p></li>
								    <li><i class="fa fa-folder" aria-hidden="true"><?php the_category(' ');?></i></li>
								    <?php if(has_tag()):   ?>
								    <li><i class="fa fa-tags" aria-hidden="true"><?php the_tags('','<span>,</span>','') ?></i></li>
			                        <?php endif; ?>
									<li class="comment-link"><a href="<?php comments_link(); ?>"> <i class="fa fa-twitch" aria-hidden="true"></i> Comment</a></li>
							    </ul>
						    </div>
					
					    </div>
					    <hr>
					    <?php endwhile ?>
					<?php else: 
					
					get_template_part('404'); ?>

				<?php	endif;
				?>
			</div>
		<!-- </section> -->
    </div>
	<div class="sidebar-content category-side">
		<?php
	get_sidebar();?>
	</div>

</div>
<?php get_footer();
?>