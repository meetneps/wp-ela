<?php get_header(); ?>
<div class="single-page-img">
	<img class="single-img" src="<?php echo esc_url( get_template_directory_uri()); ?>/images/single-page-img.png" alt="image"/>
</div> 
<div class="single-page">
	<div class="post-content">
			<?php if(have_posts()): ?>
			    <?php while(have_posts()): ?>
			    <?php  the_post(); ?>
		    	<ul>
				    <li><h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2></li>
				    <li><h5 class="author-details">Posted by <span class="author-title"><?php the_author_link(); ?></span></h5></li>
				    <?php if (has_post_thumbnail()): ?>
						<li><?php the_post_thumbnail();?></li>
					<?php endif; ?>
				    <li><p class="description"><?php the_content(); ?></p></li>
			    </ul>
		    	<?php endwhile; ?>
		    <?php else: ?>
		    <?php get_template_part('404'); ?>
			<?php endif; ?>
    </div>		
    <div class="sidebar">
    	<?php get_sidebar(); ?>
    </div>
</div>
<?php get_footer(); ?>