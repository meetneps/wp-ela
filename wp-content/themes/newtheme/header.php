
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />        
        <?php wp_head(); ?>
    </head>
<body <?php body_class(); ?>> 
	<div id="header">
		<h1>HeaderPart</h1>
		<?php wp_nav_menu( array( 'theme_location' => 'header-left-menu' ) ); ?>
	</div>