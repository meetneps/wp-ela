
<?php
  function newtheme_script_enqueue(){
 	wp_enqueue_style('customstyle',get_template_directory_uri().'/css/newtheme.css');
 	
    wp_enqueue_script('script',get_template_directory_uri().'/js/newtheme.js',array(),false,true);
 }
 add_action('wp_enqueue_scripts','newtheme_script_enqueue');

  function newtheme_custome_menus(){
 	add_theme_support('menus');
 	 register_nav_menus(
    array(
      'header-left-menu' => __( 'Header Left Menu' ,'newtheme'),
      'header-right-menu' =>__('Header Right Menu','newtheme'),
      'footer-menu' => __( 'Footer Menu','newtheme' ),
      'header-bottom' =>__('Header Bottom Menu','newtheme')

    )
  );
 }
 add_action('init','newtheme_custome_menus');
 
 function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Right sidebar',
		'id'            => 'right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );

?>