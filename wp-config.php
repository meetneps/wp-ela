<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wp_dba');

/** MySQL database password */
define('DB_PASSWORD', 'asdf');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R?1NG3C9T.C>$8Q&OJkkD+.@Gp[L ,8O%uzaLHl3),$ ~b([<Qb|}+SpZ<!2GO!.');
define('SECURE_AUTH_KEY',  'Tni%]I<fAe cz%JL/>C2axYtMoBU~|YBf({@(%>D]g95Wh9s:$kmpWZfe/:72X#g');
define('LOGGED_IN_KEY',    '2KxXAe(5Ss]w@FPvgQwX)u2TCiC(8sf4}Fzmg=)^(v::opan-K9We}s;gwPWmpf;');
define('NONCE_KEY',        'I|G7ri88b]yEmYAd)[K>9S:;xe9j2Ze)g,I$hK[nCR]v66m+Na$*V@DL {#h^IhJ');
define('AUTH_SALT',        'X5{F^B>PV@Hk[1X[3NCE^?^#aMrBJ{F_jS<b(&6GCH~T9kD<~MpI@p3~1l$O1?W9');
define('SECURE_AUTH_SALT', 'E[UP{!&p|#~|$N:T% j-bG^O/!&FrvfTN1P],8@`c7=qOFa z)^^OQ3Se`I^nLQ|');
define('LOGGED_IN_SALT',   ']$g!Lzknh~tHOV9/au30<}D,7`P%zB/sA+8qA$Ai`dgrW|![^m173PH-.:wMC!af');
define('NONCE_SALT',       'c`QUWhUQ+lu[cd/FU5m@Cg@Thu{)sQ@2[]Bm$f<H2M>o;h{^sP@!Mqd3NLn#|9gp');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
